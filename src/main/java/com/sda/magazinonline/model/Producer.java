package com.sda.magazinonline.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity(name = "Producer")
@Table(name = "producer")
public class Producer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
    @Column(name = "name")
    String name;
    @ManyToOne
    @Column(name = "products")
    List<Product> products;
    @Column(name = "rating")
    int rating;

    public Producer() {
        // empty constructor by default
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producer producer = (Producer) o;
        return rating == producer.rating &&
                Objects.equals(id, producer.id) &&
                Objects.equals(name, producer.name) &&
                Objects.equals(products, producer.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, products, rating);
    }

    @Override
    public String toString() {
        return "Producer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", products=" + products +
                ", rating=" + rating +
                '}';
    }
}
