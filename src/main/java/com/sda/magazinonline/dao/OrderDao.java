package com.sda.magazinonline.dao;

import com.sda.magazinonline.config.StoreConfig;
import com.sda.magazinonline.model.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class OrderDao {

    // CRUD => CREATE

    public Order create(Order order) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Order orderSaved = (Order) session.save(order);

            transaction.commit();
            session.close();

            return orderSaved;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Selected order was not saved!");
            e.printStackTrace();
        }

        return null;
    }

    // CRUD => READ

    public Order findById(Long id) {
        try {
            Session session = StoreConfig.getSessionFactory().openSession();

            Order orderFound = session.find(Order.class, id);

            session.close();
            return orderFound;
        } catch (Exception e) {
            System.out.println("Order " + id + " was not found!");
            e.printStackTrace();
        }

        return null;
    }

    public List<Order> findAll() {
        Session session = StoreConfig.getSessionFactory().openSession();

        List<Order> orders = (List<Order>) session.createQuery("from order").list();
        return orders;
    }

    // CRUD => UPDATE

    public boolean update(Order order) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.update(order);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Order " + order.getId() + " was not updated!");
            e.printStackTrace();
        }

        return false;
    }

    // CRUD => DELETE

    public boolean delete(Order order) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(order);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Order " + order.getId() + " was not deleted!");
            e.printStackTrace();
        }

        return false;
    }
}
