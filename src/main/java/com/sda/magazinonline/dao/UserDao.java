package com.sda.magazinonline.dao;

import com.sda.magazinonline.config.StoreConfig;
import com.sda.magazinonline.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class UserDao {

    // CRUD => CREATE

    public User create(User user) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            User userSaved = (User) session.save(user);

            transaction.commit();
            session.close();

            return userSaved;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Selected user was not saved!");
            e.printStackTrace();
        }

        return null;
    }

    // CRUD => READ

    public User findById(Long id) {
        try {
            Session session = StoreConfig.getSessionFactory().openSession();

            User userFound = session.find(User.class, id);

            session.close();

            return userFound;
        } catch (Exception e) {
            System.out.println("User " + id + " was not found!");
            e.printStackTrace();
        }

        return null;
    }

    public List<User> findAll() {
        Session session = StoreConfig.getSessionFactory().openSession();

        List<User> users = (List<User>) session.createQuery("from user").list();
        return users;
    }

    // CRUD => UPDATE

    public boolean update(User user) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.update(user);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("User " + user.getId() + " was not updated!");
            e.printStackTrace();
        }

        return false;
    }

    // CRUD => DELETE

    public boolean delete(User user) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(user);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("User " + user.getId() + " was not deleted!");
            e.printStackTrace();
        }

        return false;
    }
}
