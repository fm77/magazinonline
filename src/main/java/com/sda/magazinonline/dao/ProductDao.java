package com.sda.magazinonline.dao;

import com.sda.magazinonline.config.StoreConfig;
import com.sda.magazinonline.model.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ProductDao {

    // CRUD => CREATE

    public Product create(Product product) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Product productSaved = (Product) session.save(product);

            transaction.commit();
            session.close();

            return productSaved;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Selected product was not saved!");
            e.printStackTrace();
        }

        return null;
    }

    // CRUD => READ

    public Product findById(Long id) {
        try {
            Session session = StoreConfig.getSessionFactory().openSession();

            Product productFound = session.find(Product.class, id);

            session.close();
            return productFound;
        } catch (Exception e) {
            System.out.println("Product " + id + " was not found!");
            e.printStackTrace();
        }

        return null;
    }

    public List<Product> findAll() {
        Session session = StoreConfig.getSessionFactory().openSession();

        List<Product> products = (List<Product>) session.createQuery("from product").list();
        return products;
    }

    // CRUD => UPDATE

    public boolean update(Product product) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.update(product);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Product " + product.getId() + " was not updated!");
            e.printStackTrace();
        }

        return false;
    }

    // CRUD => DELETE

    public boolean delete(Product product) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(product);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Product " + product.getId() + " was not deleted!");
            e.printStackTrace();
        }

        return false;
    }
}
