package com.sda.magazinonline.dao;

import com.sda.magazinonline.config.StoreConfig;
import com.sda.magazinonline.model.Producer;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ProducerDao {

    // CRUD => CREATE

    public Producer create(Producer producer) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Producer producerSaved = (Producer) session.save(producer);

            transaction.commit();
            session.close();

            return producerSaved;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Selected producer was not saved!");
            e.printStackTrace();
        }

        return null;
    }

    // CRUD => READ

    public Producer findById(Long id) {
        try {
            Session session = StoreConfig.getSessionFactory().openSession();

            Producer producerFound = session.find(Producer.class, id);

            session.close();

            return producerFound;
        } catch (Exception e) {
            System.out.println("Producer " + id + " was not found!");
            e.printStackTrace();
        }

        return null;
    }

    public List<Producer> findAll() {
        Session session = StoreConfig.getSessionFactory().openSession();

        List<Producer> producers = (List<Producer>) session.createQuery("from producer").list();
        return producers;
    }

    // CRUD => UPDATE

    public boolean update(Producer producer) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.update(producer);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Producer " + producer.getId() + " was not updated!");
            e.printStackTrace();
        }

        return false;
    }

    // CRUD => DELETE

    public boolean delete(Producer producer) {
        Transaction transaction = null;
        try {
            Session session = StoreConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(producer);

            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("Producer " + producer.getId() + " was not deleted!");
            e.printStackTrace();
        }

        return false;
    }
}
