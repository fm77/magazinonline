package com.sda.magazinonline.controller;

import com.sda.magazinonline.model.User;
import com.sda.magazinonline.service.UserService;

import java.util.logging.Logger;

public class UserController {

    private static final Logger logger = Logger.getLogger(UserController.class.getName());

    UserService userService = new UserService();

    // CRUD => CREATE

    public boolean saveUser(User user) {
        logger.info("Saving user " + user.getFirstName() + " " + user.getLastName());
        return userService.saveUser(user);
    }

    // CRUD => READ

    public User findUser(Long id) {
        User userFound = userService.getUserById(id);
        logger.info("Founded user is " + userFound.toString());
        return userFound;
    }

    // CRUD => UPDATE

    public boolean updateUser(User user) {
        boolean isUserUpdated = userService.updateUser(user);
        if (isUserUpdated) {
            logger.info("User " + user.getId() + " was updated!");
        } else {
            logger.info("User " + user.getId() + " was not updated!");
        }
        return isUserUpdated;
    }

    // CRUD => DELETE

    public boolean deleteUser(User user) {
        logger.info("Deleting user...");
        boolean isUserDeleted = userService.deleteUser(user);
        if (isUserDeleted) {
            logger.info("Deleted user is " + user.getFirstName() + " " + user.getLastName());
        } else {
            logger.info("Error deleting user " + user.getFirstName() + " " + user.getLastName());
        }
        return isUserDeleted;
    }
}
