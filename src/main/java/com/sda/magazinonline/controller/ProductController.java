package com.sda.magazinonline.controller;

import com.sda.magazinonline.model.Product;
import com.sda.magazinonline.service.ProductService;

import java.util.List;
import java.util.logging.Logger;

public class ProductController {

    private static final Logger logger = Logger.getLogger(ProductController.class.getName());

    ProductService productService = new ProductService();

    // CRUD => CREATE

    public boolean saveProduct(Product product) {
        logger.info("Saving product " + product.getName());
        return productService.saveProduct(product);
    }

    public boolean saveProducts(List<Product> products) {
        logger.info("Saving all products!");
        return productService.saveProducts(products);
    }

    // CRUD => READ

    public Product findProduct(Long id) {
        Product productFound = productService.getProductById(id);
        logger.info("Found product: " + productFound.toString());
        return productFound;
    }

    // CRUD => UPDATE

    public boolean updateProduct(Product product) {
        boolean isProductUpdated = productService.updateProduct(product);
        if (isProductUpdated) {
            logger.info("Product " + product.getId() + " was updated!");
        } else {
            logger.info("Product " + product.getId() + " was not updated!");
        }
        return isProductUpdated;
    }

    // CRUD => DELETE

    public boolean deleteProduct(Product product) {
        logger.info("Deleting product...");
        boolean isProductDeleted = productService.deleteProduct(product);
        if (isProductDeleted) {
            logger.info("Product " + product.getName() + " was deleted!");
        } else {
            logger.info("Error deleting product " + product.getName());
        }
        return isProductDeleted;
    }
}
