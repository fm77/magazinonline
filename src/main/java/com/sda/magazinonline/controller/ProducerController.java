package com.sda.magazinonline.controller;

import com.sda.magazinonline.model.Producer;
import com.sda.magazinonline.service.ProducerService;

import java.util.logging.Logger;

public class ProducerController {

    private static final Logger logger = Logger.getLogger(ProducerController.class.getName());

    ProducerService producerService = new ProducerService();

    // CRUD => CREATE

    public boolean saveProducer(Producer producer) {
        logger.info("Saving producer " + producer.getName());
        return producerService.saveProducer(producer);
    }

    // CRUD => READ

    public Producer findProducer(Long id) {
        Producer producerFound = producerService.getProducerById(id);
        logger.info("Producerul gasit: " + producerFound.toString());
        return producerFound;
    }

    // CRUD => UPDATE

    public boolean updateProducer(Producer producer) {
        boolean isProducerUpdated = producerService.updateProducer(producer);
        if (isProducerUpdated) {
            logger.info("Producer " + producer.getId() + " was updated!");
        } else {
            logger.info("Producer " + producer.getId() + " was not updated!");
        }
        return isProducerUpdated;
    }

    // CRUD => DELETE

    public boolean deleteProducer(Long id) {
        logger.info("Deleting producer...");
        Producer producerSearched = producerService.getProducerById(id);
        boolean isProducerDeleted = producerService.deleteProducer(producerSearched);
        if (isProducerDeleted) {
            logger.info("Producer " + producerSearched.getName() + " was deleted!");
        } else {
            logger.info("Error deleting producer " + producerSearched.getName());
        }
        return isProducerDeleted;
    }
}
