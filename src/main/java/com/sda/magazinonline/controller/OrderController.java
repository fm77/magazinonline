package com.sda.magazinonline.controller;

import com.sda.magazinonline.model.Order;
import com.sda.magazinonline.service.OrderService;

import java.util.logging.Logger;

public class OrderController {

    private static final Logger logger = Logger.getLogger(OrderController.class.getName());

    OrderService orderService = new OrderService();

    // CRUD => CREATE

    public boolean saveOrder(Order order) {
        logger.info("Saving order " + order.getId());
        return orderService.saveOrder(order);
    }

    // CRUD => READ

    public Order findOrder(Long id) {
        Order orderFound = orderService.getOrderById(id);
        logger.info("Found order: " + orderFound.toString());
        return orderFound;
    }

    // CRUD => UPDATE

    public boolean updateOrder(Order order) {
        boolean isOrderUpdated = orderService.updateOrder(order);
        if (isOrderUpdated) {
            logger.info("Order " + order.getId() + " was updated!");
        } else {
            logger.info("Order " + order.getId() + " was not updated!");
        }
        return isOrderUpdated;
    }

    // CRUD => DELETE

    public boolean deleteOrder(Long id) {
        logger.info("Deleting order...");
        Order orderSearched = orderService.getOrderById(id);
        boolean isOrderDeleted = orderService.deleteOrder(orderSearched);
        if (isOrderDeleted) {
            logger.info("Order " + orderSearched.getId() + " was deleted!");
        } else {
            logger.info("Error deleting order " + orderSearched.getId());
        }
        return isOrderDeleted;
    }

}
