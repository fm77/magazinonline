package com.sda.magazinonline.main;

import com.sda.magazinonline.controller.OrderController;
import com.sda.magazinonline.controller.ProducerController;
import com.sda.magazinonline.controller.ProductController;
import com.sda.magazinonline.controller.UserController;

import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    private static ProducerController producerController = new ProducerController();
    private static ProductController productController = new ProductController();
    private static OrderController orderController = new OrderController();
    private static UserController userController = new UserController();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        // todo populate database

        // STARTING DATABASE
        System.out.println("Starting database... please wait...");

        // STARTING APPLICATION
        logger.info("Starting MagazinOnline...");
        System.out.println("Welcome to MagazinOnline!");

        // MAIN MENU
        boolean exitMainMenu = false;
        do {
            displayMainMenu();
            int selectedChoice = scanner.nextInt();
            switch (selectedChoice) {
                case 0: {
                    exitMainMenu = true;
                }
                break;
                case 1: {
                    boolean exitUserMenu = false;
                    do {
                        displayUserMenu();
                        int selectedUserOperation = scanner.nextInt();
                        switch (selectedUserOperation) {
                            case 0: {
                                exitUserMenu = true;
                                System.out.println("#0 MAIN MENU sample code");
                            }
                            break;
                            case 1: {
                                System.out.println("#1 USER MENU sample code");
                            }
                            break;
                            case 2: {
                                System.out.println("#2 USER MENU sample code");
                            }
                            break;
                            case 3: {
                                System.out.println("#3 USER MENU sample code");
                            }
                            break;
                            case 4: {
                                System.out.println("#4 USER MENU sample code");
                            }
                            break;
                            default: {
                                System.out.println("Please enter a valid operation!");
                            }
                            break;
                        }
                    } while (!exitUserMenu);
                }
                break;
                case 2: {
                    boolean exitOrderMenu = false;
                    do {
                        displayOrderMenu();
                        int selectedOrderOperation = scanner.nextInt();
                        switch (selectedOrderOperation) {
                            case 0: {
                                exitOrderMenu = true;
                                System.out.println("#0 MAIN MENU sample code");
                            }
                            break;
                            case 1: {
                                System.out.println("#1 ORDER MENU sample code");
                            }
                            break;
                            case 2: {
                                System.out.println("#2 ORDER MENU sample code");
                            }
                            break;
                            case 3: {
                                System.out.println("#3 ORDER MENU sample code");
                            }
                            break;
                            case 4: {
                                System.out.println("#4 ORDER MENU sample code");
                            }
                            break;
                            default: {
                                System.out.println("Please enter a valid operation!");
                            }
                            break;
                        }
                    } while (!exitOrderMenu);
                }
                break;
                case 3: {
                    boolean exitProductMenu = false;
                    do {
                        displayProductMenu();
                        int selectedProductOperation = scanner.nextInt();
                        switch (selectedProductOperation) {
                            case 0: {
                                exitProductMenu = true;
                                System.out.println("#0 MAIN MENU sample code");
                            }
                            break;
                            case 1: {
                                System.out.println("#1 PRODUCT MENU sample code");
                            }
                            break;
                            case 2: {
                                System.out.println("#2 PRODUCT MENU sample code");
                            }
                            break;
                            case 3: {
                                System.out.println("#3 PRODUCT MENU sample code");
                            }
                            break;
                            case 4: {
                                System.out.println("#4 PRODUCT MENU sample code");
                            }
                            break;
                            default: {
                                System.out.println("Please enter a valid operation!");
                            }
                            break;
                        }
                    } while (!exitProductMenu);
                }
                break;
                case 4: {
                    boolean exitProducerMenu = false;
                    do {
                        displayProducerMenu();
                        int selectedProducerOperation = scanner.nextInt();
                        switch (selectedProducerOperation) {
                            case 0: {
                                exitProducerMenu = true;
                                System.out.println("#0 MAIN MENU sample code");
                            }
                            break;
                            case 1: {
                                System.out.println("#1 PRODUCER MENU sample code");
                            }
                            break;
                            case 2: {
                                System.out.println("#2 PRODUCER MENU sample code");
                            }
                            break;
                            case 3: {
                                System.out.println("#3 PRODUCER MENU sample code");
                            }
                            break;
                            case 4: {
                                System.out.println("#4 PRODUCER MENU sample code");
                            }
                            break;
                            default: {
                                System.out.println("Please enter a valid operation!");
                            }
                            break;
                        }
                    } while (!exitProducerMenu);
                }
                break;
                default: {
                    System.out.println("Please make a valid choice!");
                }
                break;
            }
        } while (!exitMainMenu);

        System.out.println("Thank you!");

    }

    private static void displayProducerMenu() {
        System.out.println("PRODUCER MENU");
        System.out.println("-------------");
        System.out.println("0. Main menu");
        System.out.println("1. Save producer");
        System.out.println("2. Find producer");
        System.out.println("3. Update producer");
        System.out.println("4. Delete producer");
        System.out.println("Please select an operation: ");
    }

    private static void displayProductMenu() {
        System.out.println("PRODUCT MENU");
        System.out.println("------------");
        System.out.println("0. Main menu");
        System.out.println("1. Save product");
        System.out.println("2. Find product");
        System.out.println("3. Update product");
        System.out.println("4. Delete product");
        System.out.println("Please select an operation: ");
    }

    private static void displayOrderMenu() {
        System.out.println("ORDER MENU");
        System.out.println("----------");
        System.out.println("0. Main menu");
        System.out.println("1. Save order");
        System.out.println("2. Find order");
        System.out.println("3. Update order");
        System.out.println("4. Delete order");
        System.out.println("Please select an operation:");
    }

    private static void displayUserMenu() {
        System.out.println("USER MENU");
        System.out.println("---------");
        System.out.println("0. Main menu");
        System.out.println("1. Save user");
        System.out.println("2. Find user");
        System.out.println("3. Update user");
        System.out.println("4. Delete user");
        System.out.println("Please select an operation:");
    }

    private static void displayMainMenu() {
        System.out.println("MAIN MENU");
        System.out.println("---------");
        System.out.println("0. Exit");
        System.out.println("1. Users");
        System.out.println("2. Orders");
        System.out.println("3. Products");
        System.out.println("4. Producers");
        System.out.println("Please make your choice:");
    }
}