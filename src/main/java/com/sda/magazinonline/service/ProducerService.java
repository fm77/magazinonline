package com.sda.magazinonline.service;

import com.sda.magazinonline.dao.ProducerDao;
import com.sda.magazinonline.model.Producer;

import java.util.List;

public class ProducerService {

    ProducerDao producerDao = new ProducerDao();

    // CRUD => CREATE

    public boolean saveProducer(Producer producer) {
        Producer producerSaved = producerDao.create(producer);

        if (producerSaved != null) {
            System.out.println("Producer was saved!");

            return true;
        }

        return false;
    }

    public void saveProducers(List<Producer> producerList) {
        System.out.println("Saving producers from producer list!");

        for (Producer producer : producerList) {
            Producer producerSaved = producerDao.create(producer);
        }
    }

    // CRUD => READ

    public Producer getProducerById(Long id) {
        System.out.println("Searching for the producer " + id);

        Producer producerFound = producerDao.findById(id);

        return producerFound;
    }

    public List<Producer> getAll() {
        System.out.println("Searching for all producers to display!");

        List<Producer> producerList = producerDao.findAll();

        return producerList;
    }

    // CRUD => UPDATE

    public boolean updateProducer(Producer producer) {
        System.out.println("Producer will be updated!");
        boolean isProducerUpdated = producerDao.update(producer);
        return isProducerUpdated;
    }

    // CRUD => DELETE

    public boolean deleteProducer(Producer producer) {
        System.out.println("Producer will be deleted!");
        boolean isProducerDeleted = producerDao.delete(producer);
        return isProducerDeleted;
    }
}
