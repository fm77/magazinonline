package com.sda.magazinonline.service;

import com.sda.magazinonline.dao.OrderDao;
import com.sda.magazinonline.model.Order;

import java.util.List;

public class OrderService {

    OrderDao orderDao = new OrderDao();

    // CRUD => CREATE

    public boolean saveOrder(Order order) {
        Order orderSaved = orderDao.create(order);

        if (orderSaved != null) {
            System.out.println("Order was saved!");

            return true;
        }

        return false;
    }

    public void saveOrders(List<Order> orderList) {
        System.out.println("Saving orders from order list!");

        for (Order order : orderList) {
            Order orderSaved = orderDao.create(order);
        }
    }

    // CRUD => READ

    public Order getOrderById(Long id) {
        System.out.println("Searching for the order " + id);

        Order orderFound = orderDao.findById(id);

        return orderFound;
    }

    public List<Order> getAll() {
        System.out.println("Searching for all orders to display!");

        List<Order> orderList = orderDao.findAll();

        return orderList;
    }

    // CRUD => UPDATE

    public boolean updateOrder(Order order) {
        System.out.println("Order will be updated!");
        boolean isOrderUpdated = orderDao.update(order);
        return isOrderUpdated;
    }

    // CRUD => DELETE

    public boolean deleteOrder(Order order) {
        System.out.println("Order will be deleted!");
        boolean isOrderDeleted = orderDao.delete(order);
        return isOrderDeleted;
    }
}
