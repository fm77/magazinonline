package com.sda.magazinonline.service;

import com.sda.magazinonline.dao.UserDao;
import com.sda.magazinonline.model.User;

import java.util.List;

public class UserService {

    UserDao userDao = new UserDao();

    // CRUD => CREATE

    public boolean saveUser(User user) {
        User userSaved = userDao.create(user);

        if (userSaved != null) {
            System.out.println("User was saved!");

            return true;
        }

        return false;
    }

    public void saveUsers(List<User> userList) {
        System.out.println("Saving users from user list!");

        for (User user : userList) {
            User userSaved = userDao.create(user);
        }
    }

    // CRUD => READ

    public User getUserById(Long id) {
        System.out.println("Searching for the user " + id);

        User userFound = userDao.findById(id);

        return userFound;
    }

    public List<User> getAll() {
        System.out.println("Searching for all users to display!");

        List<User> userList = userDao.findAll();

        return userList;
    }

    // CRUD => UPDATE

    public boolean updateUser(User user) {
        System.out.println("User will be updated!");
        boolean isUserUpdated = userDao.update(user);
        return isUserUpdated;
    }

    // CRUD => DELETE

    public boolean deleteUser(User user) {
        System.out.println("User will be deleted!");
        boolean isUserDeleted = userDao.delete(user);
        return isUserDeleted;
    }

}
