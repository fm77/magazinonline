package com.sda.magazinonline.service;

import com.sda.magazinonline.dao.ProductDao;
import com.sda.magazinonline.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductService {

    ProductDao productDao = new ProductDao();

    // CRUD => CREATE

    public boolean saveProduct(Product product) {
        Product productSaved = productDao.create(product);

        if (productSaved != null) {
            System.out.println("Product " + product.getName() + " was saved!");

            return true;
        }
        return false;
    }

    public boolean saveProducts(List<Product> productList) {
        System.out.println("Saving products from product list!");

        List<Product> savedProductsList = new ArrayList<Product>();

        for (Product product : productList) {
            Product productSaved = productDao.create(product);
            savedProductsList.add(productSaved);
        }

        if (productList.size() == savedProductsList.size()) {
            return true;
        }
        return false;
    }

    // CRUD => READ

    public Product getProductById(Long id) {
        System.out.println("Searching for the product " + id);

        Product productFound = productDao.findById(id);

        return productFound;
    }

    public List<Product> getAll() {
        System.out.println("Searching for all products to display!");

        List<Product> productList = productDao.findAll();

        return productList;
    }

    // CRUD => UPDATE

    public boolean updateProduct(Product product) {
        System.out.println("Product will be updated!");
        boolean isProductUpdated = productDao.update(product);
        return isProductUpdated;
    }

    // CRUD => DELETE

    public boolean deleteProduct(Product product) {
        System.out.println("Product will be deleted!");
        boolean isProductDeleted = productDao.delete(product);
        return isProductDeleted;
    }
}
